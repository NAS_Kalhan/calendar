from tkinter import *
import calendar


class Calender:

    def __init__(self,master):
        self.master = master
        master.title('SK Version 2.0')
        self.screen = Entry(master,state='normal', width=15,background="yellow", foreground="blue",font=('Arial',12))
        self.screen.grid(row=1,column=0,columnspan=4,padx=5,pady=5)
        self.layout = Label(master,text='Calender',font=('Arial',16))
        self.layout.grid(row=0,column=1)
        b1 = self.create_button('Show Calender')
        b2 = self.create_button('Exit')
        b1.grid(row=3, column=1)
        b2.grid(row=5, column=1)
        self.day = None

    def create_button(self,val,width=12):
        return Button(self.master, text=val, width=width, command=lambda: self.click(val))

    def click(self,text):
        if text == 'Show Calender':
            self.get_from_screen()

        elif text == 'Exit':
            self.master.quit()

    def get_from_screen(self):
        top = self.screen.get()
        print(top)
        asm = Year(int(top))
        #self.days_layout(*self.month_layout())
        self.screen_it(asm.it_is)

    def screen_it(self,text,font=('Arial',8)):
        return Label(self.create_window(),text=text,font=font).grid(row=10, column=1, padx=20)

    def layout_frame(self,text):
        return LabelFrame(self.master,text=text)

    def month_layout(self):
        m1 = self.layout_frame('January')
        m1.grid(row=10,column=0)
        m2 = self.layout_frame('February')
        m2.grid(row=10,column=1)
        m3 = self.layout_frame('March')
        m3.grid(row=10,column=2)
        m4 = self.layout_frame('April')
        m4.grid(row=10, column=3)
        m5 = self.layout_frame('May')
        m5.grid(row=11, column=0)
        m6 = self.layout_frame('June')
        m6.grid(row=11, column=1)
        m7 = self.layout_frame('July')
        m7.grid(row=11, column=2)
        m8 = self.layout_frame('August')
        m8.grid(row=11, column=3)
        m9 = self.layout_frame('September')
        m9.grid(row=12, column=0)
        m10 = self.layout_frame('October')
        m10.grid(row=12, column=1)
        m11 = self.layout_frame('November')
        m11.grid(row=12, column=2)
        m12 = self.layout_frame('December')
        m12.grid(row=12, column=3)
        return [m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12]

    def days_layout(self,*days):
        for self.day in days:
            Label(self.day, text='S',foreground="red").grid(row=0, column=0)
            self.each_days(column=0)

            Label(self.day, text='M').grid(row=0, column=1)
            self.each_days(column=1)

            Label(self.day, text='Tu').grid(row=0, column=2)
            self.each_days(column=2)

            Label(self.day, text='W').grid(row=0, column=3)
            self.each_days(column=3)

            Label(self.day, text='Th').grid(row=0, column=4)
            self.each_days(column=4)

            Label(self.day, text='F').grid(row=0, column=5)
            self.each_days(column=5)

            Label(self.day, text='S').grid(row=0, column=6)
            self.each_days(column=6)

    def each_days(self,column):
        for each_day in range(1, 6):
            Label(self.day, text=each_day, foreground="blue").grid(row=each_day, column=column)


    def create_window(self):
        new_window = Toplevel()
        new_window.wm_title("Window")
        return new_window


class Year:
    def __init__(self,years):
        self.it_is = calendar.calendar(years)


root = Tk()
my_gui = Calender(root)
root.mainloop()




